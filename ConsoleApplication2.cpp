﻿#include <iostream>
#include <string>

using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << "test";
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Woof!\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Meow!\n";
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		cout << "Moo!\n";
	}
};

int main()
{
	Animal* p[] = { new Dog, new Cat, new Cow };

	for (auto i : p)
	{
		i->Voice();
	}
}